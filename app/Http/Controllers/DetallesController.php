<?php

namespace App\Http\Controllers;

use App\Models\detalles_venta;
use Illuminate\Http\Request;

class DetallesController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\detalles_venta  $detalles_venta
     * @return \Illuminate\Http\Response
     */
    public function show(detalles_venta $detalles_venta)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\detalles_venta  $detalles_venta
     * @return \Illuminate\Http\Response
     */
    public function edit(detalles_venta $detalles_venta)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\detalles_venta  $detalles_venta
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, detalles_venta $detalles_venta)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\detalles_venta  $detalles_venta
     * @return \Illuminate\Http\Response
     */
    public function destroy(detalles_venta $detalles_venta)
    {
        //
    }
}
